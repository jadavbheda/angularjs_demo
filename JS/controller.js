function getActiveStyle(styleName, object) {
  object = object || canvas.getActiveObject();
  if (!object) return '';

  return (object.getSelectionStyles && object.isEditing)
    ? (object.getSelectionStyles()[styleName] || '')
    : (object[styleName] || '');
};

function setActiveStyle(styleName, value, object) {
  object = object || canvas.getActiveObject();
  if (!object) return;

  if (object.setSelectionStyles && object.isEditing) {
    var style = { };
    style[styleName] = value;
    object.setSelectionStyles(style);
    object.setCoords();
  }
  else {
    object[styleName] = value;
  }

  object.setCoords();
  canvas.renderAll();
};

function getActiveProp(name) {
  var object = canvas.getActiveObject();
  if (!object) return '';

  return object[name] || '';
}

function setActiveProp(name, value) {
  var object = canvas.getActiveObject();
  if (!object) return;

  object.set(name, value).setCoords();
  canvas.renderAll();
}

function addAccessors($scope) {

  $scope.getOpacity = function() {
    return getActiveStyle('opacity') * 100;
  };
  $scope.setOpacity = function(value) {
    setActiveStyle('opacity', parseInt(value, 10) / 100);
  };

  $scope.getFill = function() {
    return getActiveStyle('fill');
  };
  $scope.setFill = function(value) {
    setActiveStyle('fill', value);
  };

  $scope.isBold = function() {
    return getActiveStyle('fontWeight') === 'bold';
  };
  $scope.toggleBold = function() {
    setActiveStyle('fontWeight',
      getActiveStyle('fontWeight') === 'bold' ? '' : 'bold');
  };
  $scope.isItalic = function() {
    return getActiveStyle('fontStyle') === 'italic';
  };
  $scope.toggleItalic = function() {
    setActiveStyle('fontStyle',
      getActiveStyle('fontStyle') === 'italic' ? '' : 'italic');
  };

  $scope.getText = function() {
    return getActiveProp('text');
  };
  $scope.setText = function(value) {
    setActiveProp('text', value);
  };

  $scope.getTextAlign = function() {
    return capitalize(getActiveProp('textAlign'));
  };
  $scope.setTextAlign = function(value) {
    setActiveProp('textAlign', value.toLowerCase());
  };

  $scope.getFontFamily = function() {
    return getActiveProp('fontFamily').toLowerCase();
  };
  $scope.setFontFamily = function(value) {
    setActiveProp('fontFamily', value.toLowerCase());
  };

  $scope.getBgColor = function() {
    return getActiveProp('backgroundColor');
  };
  $scope.setBgColor = function(value) {
    setActiveProp('backgroundColor', value);
  };

  $scope.getTextBgColor = function() {
    return getActiveProp('textBackgroundColor');
  };
  $scope.setTextBgColor = function(value) {
    setActiveProp('textBackgroundColor', value);
  };

  $scope.getStrokeColor = function() {
    return getActiveStyle('stroke');
  };
  $scope.setStrokeColor = function(value) {
    setActiveStyle('stroke', value);
  };

  $scope.getStrokeWidth = function() {
    return getActiveStyle('strokeWidth');
  };
  $scope.setStrokeWidth = function(value) {
    setActiveStyle('strokeWidth', parseInt(value, 10));
  };

  $scope.getFontSize = function() {
    return getActiveStyle('fontSize');
  };
  $scope.setFontSize = function(value) {
    setActiveStyle('fontSize', parseInt(value, 10));
  };

  $scope.getLineHeight = function() {
    return getActiveStyle('lineHeight');
  };
  $scope.setLineHeight = function(value) {
    setActiveStyle('lineHeight', parseFloat(value, 10));
  };

  $scope.getBold = function() {
    return getActiveStyle('fontWeight');
  };
  $scope.setBold = function(value) {
    setActiveStyle('fontWeight', value ? 'bold' : '');
  };

  $scope.getCanvasBgColor = function() {
    return canvas.backgroundColor;
  };
  $scope.setCanvasBgColor = function(value) {
    canvas.backgroundColor = value;
    canvas.renderAll();
  };

  $scope.addText = function() {

    var positions = returnPositions();

    var idName = $("#label_ids :selected").val();
    var thisText = $("#label_ids :selected").html();

    var textSample = new fabric.IText(thisText, {
      fontFamily: 'arial',
      scaleX: 1,
      scaleY: 1,
      lockScalingX: true,
      lockScalingY: true,
      //fontWeight: '',
      fontSize: 13, //1pt == 1.33px(use Math.ceil())
      originX: 'left',
      left: positions.left,
      top: positions.top,
      hasRotatingPoint: false,
      centerTransform: true
    });

    /* Extend object with custom property */
    textSample.toObject = (function(toObject) {
      return function() {
        return fabric.util.object.extend(toObject.call(this), {
          id: this.id
        });
      };
    })(textSample.toObject);

    textSample.id = idName;
    /* End of code. */
    canvas.add(textSample);

    $("#selectLabel").modal("hide");

  };

  $scope.confirmClear = function() {
    if (confirm('Are you sure?')) {
      canvas.clear();
    }
  };

  $scope.rasterize = function() {
    if (!fabric.Canvas.supports('toDataURL')) {
      alert('This browser doesn\'t provide means to serialize canvas to an image');
    }
    else {
      //window.open(canvas.toDataURL('png'));
      //$("#TestingImage").attr('src', canvas.toDataURL('png'));
    }
  };

  $scope.rasterizeSVG = function() {
    /*window.open(
      'data:image/svg+xml;utf8,' +
      encodeURIComponent(canvas.toSVG()));*/
      
      //$("#TestingSVG").html(canvas.toSVG());
  };

  $scope.rasterizeJSON = function(pharma_id, width, height, type, format) {
    //To load canvas from JSON:
    //canvas.loadFromJSON(json,canvas.renderAll.bind(canvas));
    var patient = $('#canvas').attr('patient-id');
    var drug = $('#canvas').attr('drug-id');
    data = {};
    data['pharmacy'] = pharma_id;
    data['content'] = JSON.stringify(canvas);
    data['width'] = width;
    data['height'] = height;
    data['type'] = type;
    data['patient'] = patient;
    data['drug'] = drug;
    data['page_format'] = format;
    if (type == 'edit') {
      var canvas_id = $('#canvas').attr('data-id');
      data['canvas_id'] = canvas_id;
      var dose_id = $('#canvas').attr('dose-id');
      data['dose_id'] = dose_id;
    }
    var canvas_data = canvas.getObjects();
    for(var i = 0, len = canvas_data.length; i < len; i++){
      if(canvas_data[i].id == "dose_date"){
       data['created_on'] = canvas_data[i].text;
      }
      else if(canvas_data[i].id == 'amount'){
            data['amount'] = canvas_data[i].text;
        }
    }
    $.ajax({
           data: data,
           type: "post",
           url: "save_sample.php",
           success: function(data){
                alert('Sample is successfully saved.');
           }
    });
  };

  $scope.printLabel = function(){

    var printData = canvas.toSVG();
    var popupWin = window.open('', '_blank');
    popupWin.document.open();
    popupWin.document.write('<html><body onload="window.print()">'+printData+"</html>");
    popupWin.document.close();
  };

  $scope.getSelected = function() {
    return canvas.getActiveObject();
  };

  $scope.removeSelected = function() {
    var activeObject = canvas.getActiveObject(),
        activeGroup = canvas.getActiveGroup();

    if (activeGroup) {
      var objectsInGroup = activeGroup.getObjects();
      canvas.discardActiveGroup();
      objectsInGroup.forEach(function(object) {
        canvas.remove(object);
      });
    }
    else if (activeObject) {
      canvas.remove(activeObject);
    }
  };

}

function watchCanvas($scope) {

  function updateScope() {
    $scope.$$phase || $scope.$digest();
    canvas.renderAll();
  }

  canvas
    .on('object:selected', updateScope)
    .on('group:selected', updateScope)
    .on('path:created', updateScope)
    .on('selection:cleared', updateScope);
}

druginfo.controller('CanvasControls', function($scope) {

  $scope.canvas = canvas;
  $scope.getActiveStyle = getActiveStyle;

  addAccessors($scope);
  watchCanvas($scope);
});

/*
Returns next blank left, top positions
*/
function returnPositions(){
    var canvasObjects = canvas.toObject().objects;

    var left = 0;
    var top = 0;

    for(var i = 0, len = canvasObjects.length; i < len; i++){

      if(canvasObjects[i].left <= left){
        left = canvasObjects[i].left + canvasObjects[i].width * canvasObjects[i].scaleX;
      }

      if(canvasObjects[i].top <= top){
        top = canvasObjects[i].top + canvasObjects[i].height * canvasObjects[i].scaleY;
      }

    }

    return {"left": left, "top": top};
}

function addImage(imageData) {

  var positions = returnPositions();

  fabric.Image.fromURL(imageData, function(image) {

    image.set({
      left: positions.left,
      top: positions.top,
      //lockScalingX: true,
      //lockScalingY: true,
      width: 180,
      height: 179,
      //lockMovementX: true,
      //lockMovementY: true,
      lockRotation: true
    });
    //.scale()
    //.setCoords();

    /* Extend object with custom property */
    image.toObject = (function(toObject) {
      return function() {
        return fabric.util.object.extend(toObject.call(this), {
          id: this.id
        });
      };
    })(image.toObject);

    image.id = "logo";
    /* End of code. */

    canvas.add(image);
  });
};

var addImage1 = function(item) {
  if (item.files && item.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function (e) {
          addImage(e.target.result);
      }
      reader.readAsDataURL(item.files[0]);
  }
};

$('#Change-selected').click( function(){
    type = canvas.getActiveObject().get('type');
    if (type=="image") {
      $('#image1').click();
    }
    else{
      //this.disabled = 'disabled';
    }
});

function changeImage(item){

  var selectedImage = canvas.getActiveObject();
  if (item.files && item.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function (e) {
          selectedImage._element.src = e.target.result;
          canvas.renderAll();
      }
      reader.readAsDataURL(item.files[0]);
  }

}

function setTextByID(id, text){
  obj = canvas.toJSON();
  data = obj.objects;
  setTimeout(function(){
    setObjectIDCanvas(canvas, data);
  }, 500);
  for(var i = 0, len = data.length; i < len; i++){
    if(data[i].id == id){
      if (data[i].type == 'i-text') {
        data[i].text = text;
      }
      else{
        alert('The type of object is not i-text.');
      }     
    }
  }
  obj.objects = data;
  var jData = JSON.stringify(obj);
  canvas.clear();
  canvas.loadFromJSON(jData, canvas.renderAll.bind(canvas));
}

function getTextByID(id){
  var getText ='';
  obj = canvas.toJSON();
  data = obj.objects;
  for(var i = 0, len = data.length; i < len; i++){
    if(data[i].id == id){
      if (data[i].type == 'i-text') {
        getText = data[i].text;
      }
      else{
        alert('The type of object is not i-text.');
      }  
    }
  }
  return getText;
}

function getTextByID_canvas(id, canvas){
  var getText ='';
  obj = canvas.toJSON();
  data = obj.objects;
  for(var i = 0, len = data.length; i < len; i++){
    if(data[i].id == id){
      if (data[i].type == 'i-text') {
        getText = data[i].text;
      }
      else{
        alert('The type of object is not i-text.');
      }  
    }
  }
  return getText;
}

function setBlockByPos(pos_dict, id, text, canvas){
  var textSample = new fabric.IText(text, {
      fontFamily: 'arial',
      scaleX: 1,
      scaleY: 1,
      fontWeight: 'normal',
      fontSize: 16, //1pt == 1.33px(use Math.ceil())
      originX: 'left',
      left: pos_dict.left,
      top: pos_dict.top,
      hasRotatingPoint: false,
      centerTransform: true
    });

    /* Extend object with custom property */
    textSample.toObject = (function(toObject) {
      return function() {
        return fabric.util.object.extend(toObject.call(this), {
          id: this.id
        });
      };
    })(textSample.toObject);

    textSample.id = id;
    /* End of code. */ 
    //alert();
    canvas.add(textSample);    
}

function getPosByID(id, canvas){
  var pos = {};
  obj = canvas.toJSON();
  data = obj.objects;
  console.log(data);
  for(var i = 0, len = data.length; i < len; i++){
    if(data[i].id == id){
      if (data[i].type == 'i-text') {
        pos['left'] = data[i].left;
        pos['top'] = data[i].top + 30;
        console.log("dictionary pos object");
        console.log(pos);
      }
      else{
        alert('The type of object is not i-text.');
      }  
    }
  }
  return pos;
}

function setBulkTextByID(ids, texts){
  obj = canvas.toJSON();
  data = obj.objects;
  setTimeout(function(){
    setObjectIDCanvas(canvas, data);
  }, 500);
  for(var i = 0, len = data.length; i < len; i++){
    for(var j=0;j < ids.length; j++){
      if (data[i].id == ids[j]) {
        if (data[i].type == 'i-text') {
          data[i].text = texts[j];
        }
        else{
          alert('The type of object is not i-text.');
        }  
      }
    }
  }
  obj.objects = data;
  var jData = JSON.stringify(obj);
  canvas.clear();
  canvas.loadFromJSON(jData, canvas.renderAll.bind(canvas));
}

function getBulkTextByID(ids){
  var getText_arr =[];
  obj = canvas.toJSON();
  data = obj.objects;
  for(var j=0;j < ids.length; j++){
    for(var i = 0, len = data.length; i < len; i++){
      if(data[i].id == ids[j]){
        if (data[i].type == 'i-text') {
          getText_arr.push(data[i].text);
          break;
        }
        else{
          alert('The type of object is not i-text.');
        }
      }
    }
  }
  return getText_arr;
}

function changeA4CanvasSize(width, height, jsondata){
  canvas.clear();
  canvas.setHeight(height);
  canvas.setWidth(width);
  if (jsondata != null){
    var data = jsondata.objects;
    for(var i = 0, len = data.length; i < len; i++){
      if(data[i].id == "name"){
        data[i].text = pharma_name;
      }
      else if(data[i].id == 'address'){
        data[i].text = pharma_add;
      }
      else if(data[i].id == 'phone'){
        data[i].text = pharma_phone;
      }
    }
    jsondata.objects = data;
    var jData = JSON.stringify(jsondata);
    canvas.loadFromJSON(jData, canvas.renderAll.bind(canvas));
    setTimeout(function(){
      setObjectIDCanvas(canvas, data);
    }, 500);
  }
}

function changeCanvasSize(width, height, jsondata){
  canvas.clear();
  canvas.setHeight(height);
  canvas.setWidth(width);
  if (jsondata != null){
    var data = jsondata.objects;
    for(var i = 0, len = data.length; i < len; i++){
      if(data[i].id == "name"){
       data[i].text = pharma_name;
      }
      else if(data[i].id == 'address'){
            data[i].text = pharma_add;
        }
    }
    jsondata.objects = data;
    var jData = JSON.stringify(jsondata);
    canvas.loadFromJSON(jData, canvas.renderAll.bind(canvas));
    /*canvas.loadFromJSON(jData, canvas.renderAll.bind(canvas), function(o, object){
                 
    });*/
  }  
  setTimeout(function(){
    setObjectIDCanvas(canvas, data);
  }, 500);
  
}

function setObjectIDCanvas(t_canvas, t_data){
  c = t_canvas.getObjects();
    for (var i = c.length - 1; i >= 0; i--) {
        c[i].toObject = (function(toObject) {
            return function() {
              return fabric.util.object.extend(toObject.call(this), {
                id: this.id
              });
            };
          })(c[i].toObject);

          try{
            c[i].id = t_data[i].id;
            console.log("111111111111111111")
            console.log(c[i].id);
          }
          catch(e){}
    };    
}
